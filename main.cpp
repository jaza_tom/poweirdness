#include "Particle.h"

SYSTEM_THREAD(ENABLED);
SYSTEM_MODE(SEMI_AUTOMATIC);

void fooWorks(const char* targPtr = NULL){
   if(targPtr){
      Serial.println("fooWorks --> targPtr == TRUE");
   }
}

void fooBreaks(const char* targPtr = NULL){

   int textLen = strlen(targPtr);   //<--If this line comes before the if statement, then targPtr == TRUE even if it is NULL

   if(targPtr){
      Serial.println("fooBreaks --> targPtr == TRUE  !!!WHY DOES THIS HAPPEN!?!?!?!");
      Serial.printlnf("Address of targPtr = %p", targPtr);
   }
   else{
      Serial.println("fooBreaks --> targPtr == FALSE ");
   }

   // int textLen = strlen(targPtr);                        //<--If this line comes after the if statement, then targPtr == FALSE, as expected
   Serial.printlnf("strlen(targPtr) == %d", textLen);    //<--If this line gets commmented out, it doesn't matter where strlen() gets called since textLen is never used and compiler ignores it
}

void setup() // Put setup code here to run once
{
   Serial.begin(115200);
   delay(2000);

   const char* nullPointer = NULL;

   unsigned int testLen = strlen(nullPointer);
   Serial.printlnf("testLen = %u", testLen);
   if(nullPointer){
      Serial.println("THIS DOESN'T PRINT.... SO....");
   }

   fooWorks();
   fooBreaks();
}
//
//
// void loop(){
//
//
// }













//
// void printTest_Breaks(const char* targetPtr = NULL){
//
//    Serial.println("====printTest_Breaks====");
//
//    unsigned int textLength = strlen(targetPtr); //<--RUN 1 Made targetPtr == TRUE even if NULL ptr was passed (or no pointer passed... thereby using NULL default parameter as per function declaration)
//
//    if(targetPtr){
//       Serial.printlnf("targetPtr ==  TRUE  --> targetPtr -- (memory address %p) is: %s (%u chars long)", targetPtr, targetPtr, textLength);
//    }
//    else{
//       Serial.printlnf("targetPtr == FALSE  --> !targetPtr -- (memory address %p) is: %s (%u chars long)", targetPtr, targetPtr, textLength);
//    }
//    Serial.println();
// }
//
//
// void printTest_Good(const char* targetPtr = NULL){
//    Serial.println("====printTest_Good====");
//    if(targetPtr){
//       Serial.printlnf("targetPtr ==  TRUE  --> targetPtr -- (memory address %p) is: %s ", targetPtr, targetPtr);
//    }
//    else{
//       Serial.printlnf("targetPtr == FALSE  --> !targetPtr -- (memory address %p) is: %s", targetPtr, targetPtr);
//    }
//    Serial.println();
// }
//
//
// void setup(){
//
//    Serial.begin(115200);
//    delay(2000);
//
//    Serial.println(
//    "------------------------------------------\r\n"
//    "--------CALLING TEST FUNCTIONS------------\r\n"
//    "----------WITH NO PARAMETERS---------------\r\n"
//    );
//
//    printTest_Breaks();
//    printTest_Good();
//
//
//    Serial.println(
//    "------------------------------------------\r\n"
//    "--------CALLING TEST FUNCTIONS------------\r\n"
//    "---------WITH NULL PARAMETERS-------------\r\n"
//    );
//
//    const char* testPointer_null = NULL;
//    printTest_Breaks(testPointer_null);
//    printTest_Good(testPointer_null);
//
//
//    Serial.println(
//    "------------------------------------------\r\n"
//    "----------CALLING TEST FUNCTIONS----------\r\n"
//    "---------WITH INITIALIZED POINTER---------\r\n"
//    "----------------PAREMETER-----------------\r\n"
//    "------------------------------------------\r\n"
//    );
//
//    const char* testPointer_ini = "TEST TEXT";
//    printTest_Breaks(testPointer_ini);
//    printTest_Good(testPointer_ini);
//
//
//    delay(2000);
//
//    Serial.println(
//    "------------------------------------------\r\n"
//    "--------TRYING TEST FUNCTION CODE---------\r\n"
//    "-------FROM WITHIN SETUP USING NULL--------\r\n"
//    );
//
//    const char* testPointerInSetup_null = NULL;
//    const char* testPointerInSetup_nullStrLen = NULL;
//
//    strlen(testPointerInSetup_nullStrLen);
//
//    Serial.println("====printTest_Good====");
//    if(testPointerInSetup_null){
//       Serial.printlnf("testPointerInSetup_null ==  TRUE  --> testPointerInSetup_null -- (memory address %p) is: %s ", testPointerInSetup_null, testPointerInSetup_null);
//    }
//    else{
//       Serial.printlnf("testPointerInSetup_null == FALSE  --> !testPointerInSetup_null -- (memory address %p) is: %s", testPointerInSetup_null, testPointerInSetup_null);
//    }
//    Serial.println();
//
//
//
//    Serial.println("====printTest_Breaks====");
//    if(testPointerInSetup_nullStrLen){
//       Serial.printlnf("testPointerInSetup_nullStrLen ==  TRUE  --> testPointerInSetup_nullStrLen -- (memory address %p) is: %s ", testPointerInSetup_nullStrLen, testPointerInSetup_nullStrLen);
//    }
//    else{
//       Serial.printlnf("testPointerInSetup_nullStrLen == FALSE  --> !testPointerInSetup_nullStrLen -- (memory address %p) is: %s", testPointerInSetup_nullStrLen, testPointerInSetup_nullStrLen);
//    }
//    Serial.println();
//
//
// }
